section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rcx, rcx
.loop:
    cmp byte[rdi + rcx], 0
    jz .end
    inc rcx
    jmp .loop
.end:
    mov rax, rcx
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rax, 1
    mov rsi, rdi
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov byte[rsp + 1], 0
    mov rdi, rsp
    call print_string
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 10
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rdi
    sub rsp, 24
    mov rax, rdi
    mov rsi, 10
    lea rdi, [rsp + 23] ; Последний символ 0-т, 24 символа должно хватить
    mov byte[rdi], 0
.division:
    xor rdx, rdx
    div rsi
    add rdx, '0'
    dec rdi
    mov [rdi], dl
    test rax, rax
    jnz .division
.end:
    call print_string
    add rsp, 24
    pop rdi
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jnl .pos
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
.pos:
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push r12
    push r13
    mov r12, rdi
    mov r13, rsi
    call string_length
    mov rdx, rax
    mov rdi, r13
    push rdx
    call string_length
    pop rdx
    cmp rdx, rax
    jnz .false
.while:
    test rdx, rdx
    jz .true
    xor rsi, rsi
    mov sil, byte[r12]
    cmp sil, byte[r13]
    jnz .false
    inc r12
    inc r13
    dec rdx
    jmp .while
.true:
    xor rax, rax
    inc rax
    jmp .end
.false:
    xor rax, rax
.end:
    pop r12
    pop r13
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    push rax
    mov rsi, rsp
    mov rdx, 1
    syscall
    cmp rax, 0
    jl .error ; если при чтении произошла ошибка и результат отрицательный
    pop rax
    ret
.error:
    xor rax, rax
.end:  
    ret  

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

 read_word:
    push r12
    push r13
    xor r12, r12
    mov r13, rsi
    dec r13
.begin:
    push rdi
    call read_char
    pop rdi
    cmp al, 0x20
    jz .begin
    cmp al, 0x9
    jz .begin
    cmp al, 0xA
    jz .begin
    test al, al
    jz .end
.write_char:
    mov byte[rdi + r12], al
    inc r12
.check_next:
    push rdi
    call read_char
    pop rdi
    cmp al, 0x20
    jz .end
    cmp al, 0x9
    jz .end
    cmp al, 0xA
    jz .end
    test al, al
    jz .end
    cmp r12, r13
    jnz .write_char
.error:
    xor rax, rax
    jmp .fin
.end:
    mov byte[rdi + r12], 0
    mov rax, rdi
    mov rdx, r12
.fin:
    pop r13
    pop r12
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
.A:
    xor rsi, rsi
    mov sil, byte[rdi]
    test rsi, rsi
    jz .End
    cmp byte[rdi], '0'
    jl .End
    cmp byte[rdi], '9'
    jnle .End
.Add:    
    imul rax, 10
    sub rsi, '0'
    add rax, rsi
    inc rdx
    inc rdi
    jmp .A
.End:
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
.A: 
    xor rsi, rsi
    mov sil, byte[rdi]
    test rsi, rsi
    jz .Error
    cmp byte[rdi], '-'
    jz .negative
    cmp byte[rdi], '+'
    jz .positive_sign
    cmp byte[rdi], '0'
    jl .Error
    cmp byte[rdi], '9'
    jnle .Error
    jmp parse_uint
.negative:
    inc rdi
    call parse_uint
    test rdx, rdx
    jz .Error
    inc rdx
    neg rax
    jmp .End
.positive_sign:
    inc rdi
    call parse_uint
    test rdx, rdx
    jz .Error
    inc rdx
    jmp .End
.Error:
    xor rax, rax
    xor rdx, rdx
.End:
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    dec rdx
    push rdi
    push rdx
    push rsi
    call string_length
    pop rsi
    pop rdx
    pop rdi
    cmp rax, rdx
    jnle .Error
    mov r8, rax
.while:
    test r8, r8
    jz .End
    mov r9, [rdi]
    mov [rsi], r9
    inc rdi
    inc rsi
    dec r8
    jmp .while
.Error:
    xor rax, rax
    ret
.End:
    mov byte[rsi], 0
    ret
